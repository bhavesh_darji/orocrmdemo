set :application, "Jumph"
set :domain, "orocrmdemo.local"
set :deploy_to, "/home/ilogix/Workspace/orocrmdemo"
set :app_path, "app"

set :repository, "git@bitbucket.org:bhavesh_darji/orocrmdemo.git"
set :scm, :git

set :model_manager, "doctrine"

role :web, domain
role :app, domain, :primary => true

set :use_sudo, false

set  :keep_releases, 3

set :dump_assetic_assets, true
set :use_composer, true

set :shared_files, ["app/config/parameters.yml"]
set :shared_children, [app_path + "/logs", web_path + "/uploads", "vendor", app_path + "/sessions"]

set :writable_dirs, ["app/cache", "app/logs", "app/sessions"]
set :webserver_user, "www-data"
set :permission_method, :acl
set :use_set_permissions, true

logger.level = Logger::MAX_LEVEL


task :upload_parameters do
  origin_file = "app/config/parameters.yml"
  destination_file = shared_path + "/app/config/parameters.yml"

  try_sudo "mkdir -p #{File.dirname(destination_file)}"
  top.upload(origin_file, destination_file)
end

after "deploy:setup", "upload_parameters"

ssh_options[:forward_agent] = true
default_run_options[:pty] = true

before 'symfony:assetic:dump', 'bower:install'

namespace :bower do
    desc 'Run bower install'
    task :install do
      capifony_pretty_print "--> Installing bower components"
      invoke_command "sh -c 'cd #{latest_release} && bower install'"
      capifony_puts_ok
    end
end