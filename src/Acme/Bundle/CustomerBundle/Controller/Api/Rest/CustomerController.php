<?php

namespace Acme\Bundle\CustomerBundle\Controller\Api\Rest;

use Nelmio\ApiDocBundle\Annotation\ApiDoc;

use Symfony\Component\HttpFoundation\Response;

use FOS\RestBundle\Controller\Annotations\NamePrefix;
use FOS\RestBundle\Controller\Annotations\RouteResource;
use FOS\RestBundle\Routing\ClassResourceInterface;

use Oro\Bundle\SoapBundle\Controller\Api\Rest\RestController;

use Oro\Bundle\SecurityBundle\Annotation\Acl;

/**
 * @RouteResource("customer")
 * @NamePrefix("oro_api_")
 */
class CustomerController extends RestController implements ClassResourceInterface
{
    /**
     * REST DELETE
     *
     * @param int $id
     *
     * @ApiDoc(
     *      description="Delete Data",
     *      resource=true
     * )
     * @Acl(
     *      id="app_task_delete",
     *      type="entity",
     *      permission="DELETE",
     *      class="AcmeCustomerBundle:Customer"
     * )
     * @return Response
     */
    public function deleteAction($id)
    {
        echo $id; exit;
        return $this->handleDeleteRequest($id);
    }

    /**
     * {@inheritdoc}
     */
    public function getManager()
    {
        return $this->get('orocrm_customer.customer.manager.api');
    }

    /**
     * {@inheritdoc}
     */
    public function getForm()
    {
        throw new \BadMethodCallException('Form is not available.');
    }

    /**
     * {@inheritdoc}
     */
    public function getFormHandler()
    {
        throw new \BadMethodCallException('FormHandler is not available.');
    }
}
