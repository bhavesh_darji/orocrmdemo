<?php
namespace Acme\Bundle\CustomerBundle\Controller;

use Acme\Bundle\CustomerBundle\Entity\Customer;
use Acme\Bundle\CustomerBundle\Form\Type\CustomerType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Util\Codes;

use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;

/**
 * @Route("/customer")
 */
class CustomerController extends Controller
{

    /**
     * View Customer
     *
     * @Route("/view/{id}", name="orocrm_app_task_view", requirements={"id"="\d+"})
     * @Acl(
     *      id="orocrm_app_task_view",
     *      type="entity",
     *      permission="VIEW",
     *      class="AcmeCustomerBundle:Customer"
     * )
     * @Template
     */

    public function viewAction(Customer $entity)
    {
        return ['entity' => $entity];
    }

    /**
     * @Route(name="orocrm_app_task_index")
     * @Template
     * @AclAncestor("orocrm_app_task_view")
     */
    public function indexAction()
    {
        return [
            'entity_class' => $this->container->getParameter('orocrm_customer.entity.class')
        ];
    }

    /**
     * @Route("/info/{id}", name="orocrm_customer_info", requirements={"id"="\d+"})
     * @Template
     * @AclAncestor("orocrm_app_task_view")
     */
    public function infoAction(Customer $entity)
    {
        return [
            'entity' => $entity
        ];
    }

    /**
     * Edit campaign
     *
     * @Route("/update/{id}", name="orocrm_app_task_update", requirements={"id"="\d+"})
     * @Template
     * @Acl(
     *      id="orocrm_app_task_edit",
     *      type="entity",
     *      permission="EDIT",
     *      class="AcmeCustomerBundle:Customer"
     * )
     */
    public function updateAction(Customer $task)
    {
        return $this->update($task);
    }

    /**
     * Create customer
     *
     * @Route("/create", name="orocrm_app_task_create")
     * @Template("AcmeCustomerBundle:Customer:update.html.twig")
     * @Acl(
     *      id="orocrm_app_task_create",
     *      type="entity",
     *      permission="CREATE",
     *      class="AcmeCustomerBundle:Customer"
     * )
     */
    public function createAction()
    {
        return $this->update(new Customer());
    }

    /**
     * @Route("/delete/{id}", name="orocrm_app_task_delete", requirements={"id"="\d+"})
     * @Acl(
     *      id="orocrm_app_task_delete",
     *      type="entity",
     *      permission="DELETE",
     *      class="AcmeCustomerBundle:Customer"
     * )
     */
    public function deleteAction(Customer $entity)
    {
        /** @var EntityManager $em */
        $em = $this->get('doctrine.orm.entity_manager');

        $em->remove($entity);
        $em->flush();

        return new JsonResponse('', Codes::HTTP_OK);
    }

    /**
     * Process save campaign entity
     *
     * @param Customer $entity
     * @return array
     */
    protected function update(Customer $task)
    {
        return $this->get('oro_form.model.update_handler')->handleUpdate(
            $task,
            $this->get('orocrm_customer.customer.form'),
            function (Customer $task) {
                return array(
                    'route' => 'orocrm_app_task_update',
                    'parameters' => array('id' => $task->getId())
                );
            },
            function (Customer $task) {
                return array(
                    'route' => 'orocrm_app_task_view',
                    'parameters' => array('id' => $task->getId())
                );
            },
            $this->get('translator')->trans('Customer information has been saved successfully...!!!')
        );
    }
}

?>