<?php

namespace Acme\Bundle\CustomerBundle\Form\Type;

use Acme\Bundle\CustomerBundle\Entity\Customer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class CustomerType extends AbstractType {

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name','text', array('required' => true,'label'=> 'Name'))
            ->add('dueDate', 'oro_date', array('required' => false, 'label' => 'Due Date'))
            ->add('category')
            ->add('description','textarea', array('required' => true, 'label' => 'Description'))
            ->add(
                'priority',
                'choice',
                [
                    'label'   => 'Priority',
                    'choices' => [
                        Customer::PRIORITY_HIGH  => 'High',
                        Customer::PRIORITY_MID   => 'Midium',
                        Customer::PRIORITY_NOR => 'Low',
                    ]
                ]
            );
    }

    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'Acme\Bundle\CustomerBundle\Entity\Customer',
            'validation_groups' => ['Customer', 'Default']
        ]);
    }




    public function getName()
    {
        return 'orocrm_customer_form';
    }

}
?>