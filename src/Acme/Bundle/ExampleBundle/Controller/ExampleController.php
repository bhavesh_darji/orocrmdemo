<?php
namespace Acme\Bundle\ExampleBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ExampleController extends Controller
{
    /**
     * @Route("/example", name="orocrm_example_index")
     * @Template()
    */
    public function indexAction() {
        return array('name' => 'Test');
    }

}
?>