<?php

namespace MarketSnap\Bundle\ContactBundle\Form\Type;;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormEvent;
use OroCRM\Bundle\ContactBundle\Entity\Contact;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
//use Symfony\Component\Validator\Constraints\ as Assert;
use Symfony\Component\Validator\Constraints\Length;

class CustomContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName','text', array('required' => true,'label' => 'orocrm.contact.first_name.label'))
            ->add('lastName', 'text', array('required' => true, 'label' => 'orocrm.contact.last_name.label'))
            ->add(
                'email',
                'email',
                array(
                    'label'    => 'orocrm.contact.emails.label',
                    'required' => true,
                )
            )
            ->add(
                'phone',
                'text',
                array(
                    'label'    => 'orocrm.contact.phones.label',
                    'required' => true,
                    'constraints' => [ new  Length(array('min'=>10)) ]
                    )
                )
            ->add(
                'description',
                'textarea',
                array(
                    'required' => true,
                    'label' => 'orocrm.contact.description.label'
                )
            )
            ->add(
                'method',
                'translatable_entity',
                array(
                    'label'       => 'orocrm.contact.method.label',
                    'class'       => 'OroCRMContactBundle:Method',
                    'property'    => 'label',
                    'required'    => true,
                    'empty_value' => 'orocrm.contact.form.choose_contact_method'
                ))
            ->add(
                'leave_voice_mail',
                'choice',
                array(
                    'choices' => array(
                        '0' => 'No',
                        '1' => 'Yes'
                    ),
                    'required' => true,
                    'label' => 'Send Voice Mail'
                )
            );
    }

    public static function validateEventDates() {

    }
    public function getName()
    {
        return 'orocrm_customer_form';
    }
}
