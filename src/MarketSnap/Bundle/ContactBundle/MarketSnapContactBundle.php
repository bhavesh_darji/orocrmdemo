<?php

namespace MarketSnap\Bundle\ContactBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarketSnapContactBundle extends Bundle
{
  public function getParent()
  {
    return 'OroCRMContactBundle';
  }
}
