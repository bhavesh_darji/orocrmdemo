<?php

namespace MarketSnap\Bundle\ContactBundle\Controller;

use MarketSnap\Bundle\ContactBundle\Form\Type\CustomContactType;
use Oro\Bundle\ImportExportBundle\Formatter\ExcelDateTimeTypeFormatter;
use OroCRM\Bundle\ContactBundle\Entity\Contact;
use OroCRM\Bundle\ContactBundle\Entity\ContactAddress;
use Ratchet\Wamp\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use OroCRM\Bundle\ContactBundle\Entity\ContactPhone;
use OroCRM\Bundle\ContactBundle\Entity\ContactEmail;
use Symfony\Component\HttpFoundation\JsonResponse;

use OroCRM\Bundle\ContactBundle\Controller\ContactController as CustomContactController;

class ContactController extends CustomContactController
{
    public static $owner = 'admin';

    /**
     * @Route("/customForm", name="oro_crm_custom_form")
     */

    public function customFormAction(Request $request)
    {

        $okMessage      = 'Contact form successfully submitted. Thank you, I will get back to you soon!';
        $errorMessage   = 'There was an error while submitting the form. Please try again later';
        $errorEmail     = 'User Already Register With This Email.';

        $form = $this->createForm(new CustomContactType());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            try {

                $contact = new Contact();
                $res = $this->getDoctrine()->getManager();

                $user = $res->getRepository('OroUserBundle:User')->findOneByUsername(self::$owner);
                $organization = $res->getRepository('OroOrganizationBundle:Organization')->getFirst();

                $firstName          = $form['firstName']->getData();
                $lastName           = $form['lastName']->getData();
                $email              = $form['email']->getData();
                $phone              = $form['phone']->getData();
                $description        = $form['description']->getData();
                $method_name        = $form['method']->getData();
                $leave_voice_mail   = $form['leave_voice_mail']->getData();

                //Add Contact and phone
                $contact->addEmail((new ContactEmail($email))->setPrimary(true));
                $contact->addPhone((new ContactPhone($phone))->setPrimary(true));

                $contact->setFirstName($firstName);
                $contact->setLastName($lastName);
                $contact->setDescription($description);
                $contact->setMethod($method_name);
                $contact->setLeaveVoiceMail($leave_voice_mail);
                $contact->setOrganization($organization);
                $contact->setOwner($user);

                $check_email = $res->getRepository('OroCRMContactBundle:ContactEmail')->findOneBy(array('email'=> $email));
                if($check_email) {
                    $this->addFlash("error",$errorEmail);
                    return $this->render("MarketSnapContactBundle:Contact:contact.html.twig", array('form' => $form->createView()));
                }

                $res->persist($contact);
                $res->flush();
                $this->addFlash("success",$okMessage);

                return $this->redirectToRoute("oro_crm_custom_form");

            } catch (\Exception $ex) {

                $this->addFlash("error",$errorMessage);
            }
        }
        return $this->render("MarketSnapContactBundle:Contact:contact.html.twig", array('form' => $form->createView()));
    }
}
?>