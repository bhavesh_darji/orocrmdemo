<?php

namespace MarketSnap\Bundle\ContactUsBundle\Controller;

use MarketSnap\Bundle\ContactBundle\Form\Type\CustomContactType;
use Oro\Bundle\ImportExportBundle\Formatter\ExcelDateTimeTypeFormatter;
use OroCRM\Bundle\ContactBundle\Entity\Contact;
use OroCRM\Bundle\ContactBundle\Entity\ContactAddress;
use OroCRM\Bundle\ContactUsBundle\Entity\ContactRequest;
use Ratchet\Wamp\Exception;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use OroCRM\Bundle\ContactBundle\Entity\ContactPhone;
use OroCRM\Bundle\ContactBundle\Entity\ContactEmail;
use Symfony\Component\HttpFoundation\JsonResponse;


use OroCRM\Bundle\ContactUsBundle\Controller\ContactRequestController as CustomContactRequestController;

class ContactRequestController extends CustomContactRequestController
{
    public static $owner = 'admin';

    /**
     * @Route("/requestForm", name="oro_crm_request_form")
     */
    public function requestFormAction(Request $request)
    {
        return $this->render("MarketSnapContactUsBundle:ContactRequest:contact_us.html.twig");

    }

    /**
     * @Route("/create-contact/{id}", name="orocrm_create_contact")
     */
    public function createContactAction(Request $request, $id) {

        $em = $this->getDoctrine()->getManager();
        $data = $em->getRepository('OroCRMContactUsBundle:ContactRequest')->findOneBy(array('id'=>$id));
        $organization = $em->getRepository('OroOrganizationBundle:Organization')->getFirst();
        $contact = new Contact();
        $firstName          = $data->getFirstName();
        $lastName           = $data->getLastName();
        $email              = $data->getEmail();
        $phone              = $data->getPhone();

        //Add Contact and phone
        $contact->addEmail((new ContactEmail($email))->setPrimary(true));
        $contact->addPhone((new ContactPhone($phone))->setPrimary(true));

        $contact->setFirstName($firstName);
        $contact->setLastName($lastName);
        $contact->setOrganization($organization);

        $em->persist($contact);
        $em->remove($data);
        $em->flush();

        $this->addFlash("success",'Contact Created');

        return $this->redirectToRoute("orocrm_contactus_request_index");

    }
}