<?php

namespace MarketSnap\Bundle\ContactUsBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarketSnapContactUsBundle extends Bundle
{
    public function getParent()
    {
        return 'OroCRMContactUsBundle';
    }
}
