<?php
namespace MarketSnap\Bundle\ContactUsBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Oro\Bundle\EmbeddedFormBundle\Form\Type\EmbeddedFormInterface;

class ContactRequestType extends AbstractType implements EmbeddedFormInterface
{
    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return $this->getBlockPrefix();
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'orocrm_contactus_contact_request';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add(
            'case_subject',
            'text',
            array('required' => true, 'label' => 'Title')
        );

        if ($options['dataChannelField']) {
            $builder->add(
                'dataChannel',
                'orocrm_channel_select_type',
                [
                    'required' => true,
                    'label' => 'orocrm.contactus.contactrequest.data_channel.label',
                    'entities' => [
                        'OroCRM\\Bundle\\ContactUsBundle\\Entity\\ContactRequest'
                    ],
                ]
            );
        }

        $builder->add(
            'firstName',
            'text',
            ['required' => true, 'label' => 'orocrm.contactus.contactrequest.first_name.label']
        );
        $builder->add(
            'lastName',
            'text',
            ['required' => true, 'label' => 'orocrm.contactus.contactrequest.last_name.label']
        );
        $builder->add(
            'emailAddress',
            'text',
            ['required' => true, 'label' => 'orocrm.contactus.contactrequest.email_address.label']
        );
        $builder->add('phone', 'text', ['required' => false, 'label' => 'orocrm.contactus.contactrequest.phone.label']);
        $builder->add('comment', 'textarea', ['label' => 'orocrm.contactus.contactrequest.comment.label']);
        $builder->add('submit', 'submit');
    }

    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            [
            'data_class' => 'OroCRM\Bundle\ContactUsBundle\Entity\ContactRequest',
            'dataChannelField' => false
            ]
        );
    }

    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function getDefaultCss()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function getDefaultSuccessMessage()
    {
        return '<p>Form has been submitted successfully</p>{back_link}';
    }
}
