<?php

namespace MarketSnap\Bundle\SalesBundle\Handler;

use Symfony\Component\PropertyAccess\PropertyAccess;

use Oro\Bundle\EntityBundle\Form\EntityField\Handler\Processor\AbstractEntityApiHandler;

use OroCRM\Bundle\SalesBundle\Handler\OpportunityApiHandler as BaseOpportunityApiHandler;

class OpportunityApiHandler extends BaseOpportunityApiHandler
{
    const ENTITY_CLASS = 'MarketSnap\Bundle\SalesBundle\Entity\Opportunity';

    /** @var PropertyAccess **/
    protected $accessor;

    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessor();
    }

    /**
     * {@inheritdoc}
     */
    public function afterProcess($entity)
    {
        return [
            'fields' => [
                'probability' => $this->accessor->getValue($entity, 'probability')
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function getClass()
    {
        return self::ENTITY_CLASS;
    }
}
