<?php

namespace MarketSnap\Bundle\SalesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarketSnapSalesBundle extends Bundle
{
    public function getParent()
    {
        return 'OroCRMSalesBundle';
    }
}
