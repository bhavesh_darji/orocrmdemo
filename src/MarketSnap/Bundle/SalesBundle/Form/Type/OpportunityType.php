<?php

namespace MarketSnap\Bundle\SalesBundle\Form\Type;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Form\AbstractType;
use OroCRM\Bundle\SalesBundle\Form\Type\OpportunityType as BaseOpportunityType;

class OpportunityType extends BaseOpportunityType
{
    const NAME = 'orocrm_sales_opportunity';

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);

        $builder->add(
            'customer',
            'orocrm_sales_b2bcustomer_select',
            array('required' => false, 'label' => 'orocrm.sales.opportunity.customer.label')
        );


        /*$fields = ['consult_reschedule_count', 'consult_complete', 'stage'];
        foreach($fields as $field) {
            if ($builder->has($field)) {
                $builder->remove($field);
            }
        }*/

    }
    /**
     * {@inheritdoc}
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(
            array(
                'data_class' => 'OroCRM\Bundle\SalesBundle\Entity\Opportunity',
                'intention'  => 'opportunity',
                'validation_groups' => 'ms_edit'
            )
        );
    }
}
