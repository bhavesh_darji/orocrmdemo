<?php

namespace MarketSnap\Bundle\SalesBundle\Controller;

use FOS\RestBundle\Util\Codes;
use OroCRM\Bundle\SalesBundle\Entity\SalesFunnel;
use OroCRM\Bundle\ChannelBundle\Entity\Channel;
use OroCRM\Bundle\SalesBundle\Entity\Opportunity;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Doctrine\ORM\EntityManager;
use Oro\Bundle\SecurityBundle\Annotation\Acl;
use Oro\Bundle\SecurityBundle\Annotation\AclAncestor;

use OroCRM\Bundle\SalesBundle\Controller\Api\Rest\SalesFunnelController;


use OroCRM\Bundle\SalesBundle\Controller\OpportunityController as SnapOpportunityController;

/**
 * @Route("/opportunity")
 */

class OpportunityController extends SnapOpportunityController
{
    
    /**
     * @param  Opportunity $entity
     * @return array
     */
    protected function update(Opportunity $entity)
    {
        return $this->get('oro_form.model.update_handler')->handleUpdate(
            $entity,
            $this->get('orocrm_sales.opportunity.form'),
            function (Opportunity $entity) {
                return array(
                    'route' => 'orocrm_sales_opportunity_update',
                    'parameters' => array('id' => $entity->getId())
                );
            },
            function (Opportunity $entity) {
                return array(
                    'route' => 'orocrm_sales_opportunity_view',
                    'parameters' => array('id' => $entity->getId())
                );
            },
            $this->get('translator')->trans('orocrm.sales.controller.opportunity.saved.message'),
            $this->get('orocrm_sales.opportunity.form.handler')
        );
       /* return $this->get('oro_form.model.update_handler')->update(
            $entity,
            $this->get('orocrm_sales.opportunity.form'),
            $this->get('translator')->trans('orocrm.sales.controller.opportunity.saved.message'),
            $this->get('orocrm_sales.opportunity.form.handler')
        );*/
    }
}
