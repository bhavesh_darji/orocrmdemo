<?php

namespace MarketSnap\Bundle\EmbeddedFormBundle\Controller;

use Doctrine\ORM\EntityManager;
use Doctrine\Common\Util\ClassUtils;

use OroCRM\Bundle\CaseBundle\Entity\CaseEntity;
use OroCRM\Bundle\CaseBundle\Entity\CaseStatus;
use OroCRM\Bundle\ContactBundle\Entity\Contact;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

use Oro\Bundle\EmbeddedFormBundle\Entity\EmbeddedForm;
use Oro\Bundle\OrganizationBundle\Form\Type\OwnershipType;
use Oro\Bundle\EmbeddedFormBundle\Manager\EmbeddedFormManager;
use Oro\Bundle\EmbeddedFormBundle\Manager\EmbedFormLayoutManager;
use Oro\Bundle\EmbeddedFormBundle\Event\EmbeddedFormSubmitAfterEvent;
use Oro\Bundle\EmbeddedFormBundle\Event\EmbeddedFormSubmitBeforeEvent;

use Oro\Bundle\EmbeddedFormBundle\Controller\EmbedFormController as CustomEmbedFormController;

class EmbedFormController extends CustomEmbedFormController
{
   // public static $owner = 'bhavesh123';
    /**
     * @Route("/submit/{id}", name="oro_embedded_form_submit", requirements={"id"="[-\d\w]+"})
     */
    public function formAction(EmbeddedForm $formEntity, Request $request)
    {
        $defaultOwner =  $this->getParameter('owner');
        $defaultCaseStatus =  $this->getParameter('case_status');
        $defaultCaseSource =  $this->getParameter('case_source');
        $defaultCasePriority =  $this->getParameter('case_priority');

        $response = new Response();
        $response->setPublic();
        $response->setEtag($formEntity->getId() . $formEntity->getUpdatedAt()->format(\DateTime::ISO8601));
        $this->setCorsHeaders($formEntity, $request, $response);

        if ($response->isNotModified($request)) {
            return $response;
        }

        $isInline = $request->query->getBoolean('inline');

        /** @var EntityManager $em */
        $em = $this->get('doctrine.orm.entity_manager');
        /** @var EmbeddedFormManager $formManager */
        $formManager = $this->get('oro_embedded_form.manager');
        $form        = $formManager->createForm($formEntity->getFormType());

        if (in_array($request->getMethod(), ['POST', 'PUT'])) {
            $dataClass = $form->getConfig()->getOption('data_class');
            if (isset($dataClass) && class_exists($dataClass)) {
                $ref         = new \ReflectionClass($dataClass);
                $constructor = $ref->getConstructor();
                $data        = $constructor && $constructor->getNumberOfRequiredParameters()
                    ? $ref->newInstanceWithoutConstructor()
                    : $ref->newInstance();

                $form->setData($data);
            } else {
                $data = [];
            }
            $event = new EmbeddedFormSubmitBeforeEvent($data, $formEntity);
            $eventDispatcher = $this->get('event_dispatcher');
            $eventDispatcher->dispatch(EmbeddedFormSubmitBeforeEvent::EVENT_NAME, $event);
            $form->submit($request);

            $event = new EmbeddedFormSubmitAfterEvent($data, $formEntity, $form);
            $eventDispatcher->dispatch(EmbeddedFormSubmitAfterEvent::EVENT_NAME, $event);
        }

        if ($form->isValid()) {

            $entity = $form->getData();
            $res            = $this->getDoctrine()->getManager();
            $check_email    = $res->getRepository('OroCRMContactBundle:ContactEmail')->findOneBy(array('email'=> $entity->getEmail()));
            $user           = $res->getRepository('OroUserBundle:User')->findOneByUsername($defaultOwner);

            if($check_email) {

                $organization   = $em->getRepository('OroOrganizationBundle:Organization')->getFirst();
                $caseStatus     = $em->getRepository('OroCRMCaseBundle:CaseStatus')->findOneBy(array('label' => $defaultCaseStatus));
                $caseSource     = $em->getRepository('OroCRMCaseBundle:CaseSource')->findOneBy(array('label' => $defaultCaseSource));
                $casePriority   = $em->getRepository('OroCRMCaseBundle:CasePriority')->findOneBy(array('label' => $defaultCasePriority));
                $caseAssign     = $em->getRepository('OroCRMCaseBundle:CaseEntity')->findOneBy(array('relatedContact' => $check_email->getOwner()));

                $caseEntity = new CaseEntity();

                $caseEntity->setSubject($entity->getCaseSubject());
                $caseEntity->setOwner($user);
                $caseEntity->setRelatedContact($check_email->getOwner());
                $caseEntity->setOrganization($organization);
                $caseEntity->setStatus($caseStatus);
                $caseEntity->setSource($caseSource);
                $caseEntity->setPriority($casePriority);
                $caseEntity->setDescription($entity->getComment());

                if($caseAssign && $caseAssign->getAssignedTo() != '') {
                    $data = !empty($caseAssign->getAssignedTo()) ? $caseAssign->getAssignedTo() : '';
                    $caseEntity->setAssignedTo($data);
                }
                $res->persist($caseEntity);
                $res->flush();
                $redirectUrl = $this->generateUrl('oro_embedded_form_success', [
                    'id' => $formEntity->getId(),
                    'inline' => $isInline
                ]);

                $redirectResponse = new RedirectResponse($redirectUrl);
                $this->setCorsHeaders($formEntity, $request, $redirectResponse);

                return $redirectResponse;

            }

            /**
             * Set owner ID (current organization) to concrete form entity
             */
            $entityClass      = ClassUtils::getClass($entity);
            $config           = $this->get('oro_entity_config.provider.ownership');
            $entityConfig     = $config->getConfig($entityClass);
            $formEntityConfig = $config->getConfig($formEntity);

            if ($entityConfig->get('owner_type') === OwnershipType::OWNER_TYPE_ORGANIZATION) {
                $accessor = PropertyAccess::createPropertyAccessor();
                $accessor->setValue(
                    $entity,
                    $entityConfig->get('owner_field_name'),
                    $accessor->getValue($formEntity, $formEntityConfig->get('owner_field_name'))
                );
            }
            $em->persist($entity);
            $em->flush();

            $redirectUrl = $this->generateUrl('oro_embedded_form_success', [
                'id' => $formEntity->getId(),
                'inline' => $isInline
            ]);

            $redirectResponse = new RedirectResponse($redirectUrl);
            $this->setCorsHeaders($formEntity, $request, $redirectResponse);

            return $redirectResponse;
        }

        /** @var EmbedFormLayoutManager $layoutManager */
        $layoutManager = $this->get('oro_embedded_form.embed_form_layout_manager');

        $layoutManager->setInline($isInline);

        $response->setContent($layoutManager->getLayout($formEntity, $form)->render());

        return $response;
    }
}
