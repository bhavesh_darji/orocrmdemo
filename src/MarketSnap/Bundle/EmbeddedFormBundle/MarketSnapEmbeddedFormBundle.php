<?php

namespace MarketSnap\Bundle\EmbeddedFormBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarketSnapEmbeddedFormBundle extends Bundle
{
    public function getParent()
    {
        return 'OroEmbeddedFormBundle';
    }
}

?>