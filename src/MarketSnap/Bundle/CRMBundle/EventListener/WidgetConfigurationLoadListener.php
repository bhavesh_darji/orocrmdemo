<?php

namespace MarketSnap\Bundle\CRMBundle\EventListener;

use Oro\Bundle\DashboardBundle\Event\WidgetConfigurationLoadEvent;

class WidgetConfigurationLoadListener
{
    /**
     * @param WidgetConfigurationLoadEvent $event
     */
    public function onWidgetConfigurationLoad(WidgetConfigurationLoadEvent $event)
    {
        $configuration = $event->getConfiguration();

        if (!isset($configuration['route'], $configuration['route_parameters']['name'])) {
            return;
        }
        if ('quickLaunchpad' == $configuration['route_parameters']['name']) {

            unset($configuration['items']['leads']);
        }
        $event->setConfiguration($configuration);
    }
}
