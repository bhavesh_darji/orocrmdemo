<?php

namespace MarketSnap\Bundle\CRMBundle\EventListener;

use MarketSnap\Bundle\CRMBundle\EventListener\Helper\MSGridConfigurationTrait;
use Oro\Bundle\DataGridBundle\Event\BuildBefore;

class SalesProcessGridListener
{
    use MSGridConfigurationTrait;

    /**
     * @param BuildBefore $event
     */
    public function onBuildBefore(BuildBefore $event)
    {
        $config       = $event->getConfig();
        $this->removeColumn($config, 'leadName');
    }
}
