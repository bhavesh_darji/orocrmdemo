<?php

namespace MarketSnap\Bundle\CRMBundle\EventListener\Helper;

use Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration;

trait MSGridConfigurationTrait
{
    /**
     * Removes field from grid, sorting and filtering
     *
     * @param DatagridConfiguration $config
     * @param $fieldName
     */
    protected function removeColumn(DatagridConfiguration $config, $fieldName)
    {
        $config->offsetUnsetByPath(sprintf('[columns][%s]', $fieldName));
        $config->offsetUnsetByPath(sprintf('[filters][columns][%s]', $fieldName));
        $config->offsetUnsetByPath(sprintf('[sorters][columns][%s]', $fieldName));
    }
}
