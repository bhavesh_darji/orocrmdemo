<?php

namespace MarketSnap\Bundle\CRMBundle\EventListener;

use Oro\Bundle\DataGridBundle\Event\BuildBefore;
use MarketSnap\Bundle\CRMBundle\EventListener\Helper\MSGridConfigurationTrait;

class ContactGridListener
{
    use MSGridConfigurationTrait;

    /**
     * @param BuildBefore $event
     */
    public function onBuildBefore(BuildBefore $event)
    {
        $config       = $event->getConfig();
        $this->removeColumn($config, 'reportsName');
    }
}
