<?php

namespace MarketSnap\Bundle\CRMBundle\Migrations\Schema\v1_1;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtension;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtensionAwareInterface;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;
use Oro\Bundle\EntityBundle\EntityConfig\DatagridScope;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;

class MarketSnapCRMBundle implements Migration, ExtendExtensionAwareInterface
{
    /**
     * @var ExtendExtension
     */
    protected $extendExtension;

    public function setExtendExtension(ExtendExtension $extendExtension)
    {
        $this->extendExtension = $extendExtension;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->getTable('orocrm_sales_opportunity');
        $this->extendExtension->addOneToManyRelation(
            $schema,
            $table,
            'Referral',
            'orocrm_contact',
            ['first_name', 'last_name'],
            ['first_name', 'last_name'],
            ['first_name', 'last_name'],
            [
                'extend' => [
                    'owner' => ExtendScope::OWNER_CUSTOM,
                    'merge' => ['display' => true]
                ]
            ]
        );

        $table->addColumn(
            'consult_date',
            'datetime',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'merge'    => ['display' => true],
                ]
            ]
        );

        $table->addColumn(
            'consult_reschedule_count',
            'smallint',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'merge'    => ['display' => true],
                    'form' => [
                        'is_enabled' => true,
                        'form_type'  => 'number',
                    ],
                ]
            ]
        );

        $table->addColumn(
            'consult_complete',
            'boolean',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'merge'    => ['display' => true],
                    'form' => [
                        'is_enabled' => true,
                        'form_type'  => 'checkbox',
                    ],
                ]
            ]
        );

        $table->addColumn(
            'stage',
            'string',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'merge'    => ['display' => true],
                    'form' => [
                        'is_enabled' => true,
                        'form_type'  => 'choice',
                        'form_options' => [
                            'choices' => [
                                'New' => 'New',
                                'Scheduled Consult' => 'Scheduled Consult',
                                'Won' => 'Won',
                                'Lost' => 'Lost',
                                'Invalid' => 'Invalid'
                            ]
                        ]
                    ],
                ]
            ]
        );
    }
}