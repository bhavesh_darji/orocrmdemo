<?php

namespace MarketSnap\Bundle\CRMBundle\Migrations\Schema\v1_2;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtension;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtensionAwareInterface;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;
use Oro\Bundle\EntityBundle\EntityConfig\DatagridScope;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;

class MarketSnapCRMBundle implements Migration, ExtendExtensionAwareInterface
{
    /**
     * @var ExtendExtension
     */
    protected $extendExtension;

    public function setExtendExtension(ExtendExtension $extendExtension)
    {
        $this->extendExtension = $extendExtension;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->getTable('orocrm_sales_opportunity');

        /*$table->addColumn(
            'startdate',
            'date',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'merge'    => ['display' => true],
                ]
            ]
        );*/

        $table->changeColumn(
            'consult_date',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'merge'    => ['display' => true],
                ]
            ]
        );

        $table->changeColumn(
            'consult_reschedule_count',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'datagrid' => ['is_visible' => DatagridScope::IS_VISIBLE_FALSE],
                    'view'      => ['is_displayable' => false],
                    'merge'    => ['display' => false],
                    'form' => [
                        'is_enabled' => false,
                        'form_type'  => 'number',
                    ],
                ]
            ]
        );

        $table->changeColumn(
            'consult_complete',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'datagrid' => ['is_visible' => DatagridScope::IS_VISIBLE_FALSE],
                    'view'      => ['is_displayable' => false],
                    'merge'    => ['display' => false],
                    'form' => [
                        'is_enabled' => false,
                        'form_type'  => 'checkbox',
                    ],
                ]
            ]
        );

        $table->changeColumn(
            'stage',
            [
                'oro_options' => [
                    'extend' => ['owner' => ExtendScope::OWNER_CUSTOM],
                    'datagrid' => ['is_visible' => DatagridScope::IS_VISIBLE_FALSE],
                    'view'      => ['is_displayable' => false],
                    'merge'    => ['display' => false],
                    'form' => [
                        'is_enabled' => false,
                        'form_type'  => 'choice',
                        'form_options' => [
                            'choices' => [
                                'New' => 'New',
                                'Scheduled Consult' => 'Scheduled Consult',
                                'Won' => 'Won',
                                'Lost' => 'Lost',
                                'Invalid' => 'Invalid'
                            ]
                        ]
                    ],
                ]
            ]
        );
    }
}