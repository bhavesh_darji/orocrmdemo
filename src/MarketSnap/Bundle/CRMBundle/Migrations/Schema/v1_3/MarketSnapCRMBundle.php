<?php

namespace MarketSnap\Bundle\CRMBundle\Migrations\Schema\v1_3;

use Doctrine\DBAL\Schema\Schema;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtension;
use Oro\Bundle\EntityExtendBundle\Migration\Extension\ExtendExtensionAwareInterface;
use Oro\Bundle\MigrationBundle\Migration\Migration;
use Oro\Bundle\MigrationBundle\Migration\QueryBag;
use Oro\Bundle\EntityBundle\EntityConfig\DatagridScope;
use Oro\Bundle\EntityExtendBundle\EntityConfig\ExtendScope;

class MarketSnapCRMBundle implements Migration, ExtendExtensionAwareInterface
{
    /**
     * @var ExtendExtension
     */
    protected $extendExtension;

    public function setExtendExtension(ExtendExtension $extendExtension)
    {
        $this->extendExtension = $extendExtension;
    }

    /**
     * @inheritdoc
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function up(Schema $schema, QueryBag $queries)
    {
        $table = $schema->getTable('orocrm_contact');
        $table->addColumn(
            'leave_voice_mail',
            'boolean',
            [
                'oro_options' => [
                    'extend' => ['is_extend' => true],
                    'merge'    => ['display' => true],
                    'form' => [
                        'is_enabled' => true,
                        'form_type'  => 'choice',
                        'form_options' => [
                            'choices' => [
                                '0' => 'No',
                                '1' => 'Yes',
                            ]
                        ]
                    ],
                ]
            ]);
    }

}