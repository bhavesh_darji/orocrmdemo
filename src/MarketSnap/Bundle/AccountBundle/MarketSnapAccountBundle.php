<?php

namespace MarketSnap\Bundle\AccountBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarketSnapAccountBundle extends Bundle
{
    public function getParent()
    {
        return 'OroCRMAccountBundle';
    }
}
