<?php

namespace MarketSnap\Bundle\UIBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarketSnapUIBundle extends Bundle
{
    public function getParent()
    {
        return 'OroUIBundle';
    }
}
