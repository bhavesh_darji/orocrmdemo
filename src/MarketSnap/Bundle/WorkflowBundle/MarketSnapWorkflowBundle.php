<?php

namespace MarketSnap\Bundle\WorkflowBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class MarketSnapWorkflowBundle extends Bundle
{
    public function getParent()
    {
        return 'OroWorkflowBundle';
    }
}
