<?php

namespace MarketSnap\Bundle\WorkflowBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\OptionsResolver\Options;

use Oro\Bundle\WorkflowBundle\Validator\Constraints\TransitionIsAllowed;

use Oro\Bundle\WorkflowBundle\Form\Type\WorkflowTransitionType as BaseWorkflowTransitionType;

class WorkflowTransitionType extends BaseWorkflowTransitionType
{
    /**
     * Custom options:
     * - "workflow_item" - required, instance of WorkflowItem entity
     * - "transition_name" - required, name of transition
     *
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
      //  echo 'asdasd'; exit;
        $resolver->setRequired(array('workflow_item', 'transition_name'));

        $resolver->setAllowedTypes(
            array(
                'transition_name' => 'string',
            )
        );

        $resolver->setNormalizers(
            array(
                'constraints' => function (Options $options, $constraints) {
                    if (!$constraints) {
                        $constraints = array();
                    }

                    $constraints[] = new TransitionIsAllowed(
                        $options['workflow_item'],
                        $options['transition_name']
                    );

                    return $constraints;
                }
            )
        );

        $resolver->setDefaults(
            array(
                'validation_groups' => ['ms_edit']
            )
        );
    }
}
